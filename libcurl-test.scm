#!/usr/bin/csi -s
(import (chicken foreign)
        ;; (chicken io)
        ;; (chicken process-context)
        (chicken pathname)
        ;; (prefix http-client hc:)
        ;; (prefix uri-common uc:)
        )


(foreign-declare "
#include <stdio.h>
#include <curl/curl.h>
int download_file(char *url, char *outfilename) {
    CURL *curl;
    FILE *fp;
    CURLcode res;
    curl = curl_easy_init();
    if (curl) {
        fp = fopen(outfilename,\"wb\");
        curl_easy_setopt(curl, CURLOPT_URL, url);
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, NULL);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);
        res = curl_easy_perform(curl);
        curl_easy_cleanup(curl);
        fclose(fp);
    }
    return 0;
}
")


;; (call-with-output-file "downloads/chicken-5.0.0.tar.gz"
;;   (lambda (port)
;;     (display
;;      (hc:call-with-input-request
;;       (uc:uri-reference (car (command-line-arguments)))
;;       #f (cut read-string #f <>))
;;      port)))

(define (download-file url #!optional (fname (pathname-strip-directory url)))
  ((foreign-lambda int "download_file" c-string c-string)
   url fname))

(download-file "https://code.call-cc.org/releases/5.0.0/chicken-5.0.0.tar.gz")
