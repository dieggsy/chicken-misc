#!/usr/bin/csi -s
(use posix
     scsh-process)

;; (set-buffering-mode! (current-output-port) line:)

(define adjust-volume
  (do ((line (read-line) (read-line)))
      ((eof-object? line))
    (when line
      ;; (printf "Heeyyy")
      ;; (newline)
      ;; (flush-output)
      (with-output-to-file "/tmp/ipc-polybar-simple"
        (lambda () (display "hook:module/volume1"))))))

(pipe (pactl subscribe) (begin (adjust-volume)))
