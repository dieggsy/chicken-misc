#!/usr/bin/csi -s
(use srfi-18
     posix
     micro-benchmark)

(set-buffering-mode! (current-output-port) none:)

(define (main-saving)
  (define (random-char)
    (integer->char (+ 32 (random 95))))
  (display "Input: ")
  (let ((line (read-line)))
    (let loop ((char (random-char))
               (count 0))
      (cond ((= count (string-length line))
             (newline))
            ((char=? (string-ref line count) char)
             (display char)
             (thread-sleep! .02)
             (loop (random-char) (+ count 1)))
            (else
             (display char)
             (thread-sleep! .02)
             (display #\backspace)
             (loop (random-char) count))))))

(define (main-starting-over)
  (define (random-char)
    (integer->char (+ 32 (random 95))))
  (display "Input: ")
  (let* ((line (read-line))
         (len (string-length line)))
    (benchmark-measure
     (let loop ((char (random-char))
                (count 0))
       (cond ((= count len)
              (newline))
             ((char=? (string-ref line count) char)
              (display char)
              ;; (thread-sleep! .02)
              (loop (random-char) (+ count 1)))
             (else
              (display char)
              ;; (thread-sleep! .02)
              (display "\r")
              (display (make-string len #\space))
              (display "\r")
              (loop (random-char) 0))))))
  )

(main-starting-over)
