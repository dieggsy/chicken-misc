#!/usr/bin/chicken-csi -script
(import (only chicken.process-context command-line-arguments)
        (only chicken.string string-split)
        (only chicken.io read-lines)
        (only srfi-1 find)
        (only http-client call-with-input-request)
        (only uri-common uri-reference)
        (only simple-sha1 string->sha1sum))


(define pass (car (command-line-arguments)))

(define pass-sha1sum (string->sha1sum pass))

(define pass-sha1sum-prefix (substring pass-sha1sum 0 5))

(define matched
  (call-with-input-request
   (uri-reference (string-append
                   "https://api.pwnedpasswords.com/range/"
                   pass-sha1sum-prefix))
   #f
   (cut read-lines <>)))

(define found (find (lambda (suffix)
                      (string-ci=?
                       (string-append pass-sha1sum-prefix
                                      (car (string-split suffix ":")))
                       pass-sha1sum))
                    matched))

(print "       SHA1: " pass-sha1sum)
(if found
    (begin
      (print "occurrences: " (cadr (string-split found ":")))
      (print "PWNED!"))
    (print "You're safe!"))
