(use bind)

(foreign-declare "#include <libnotify/notify.h>")
;; Using straight C
(bind-file* "notify_internal.h")

(define (notify #!key (app "chicken-notify") summary body icon)
  (notify_internal app summary body icon))

(notify summary: "hey" body: "there")

;; Lower level binding:
;; gboolean -> int
;;
;; (bind "int notify_init(const char *)")
;; (bind-type NotifyNotification (c-pointer (struct "NotifyNotification")))
;; (bind "NotifyNotification * notify_notification_new(const char *, const char *, const char *)")
;; (bind-type GError (c-pointer (struct "GError")))
;; (bind "int notify_notification_show(NotifyNotification *,GError *)")

;; (define notify-init
;;   (foreign-lambda int "notify_init" (const c-string)))

;; (define notify-notification-new
;;   (foreign-lambda (c-pointer (struct _NotifyNotification)) "notify_notification_new"
;;     (const c-string)
;;     (const c-string)
;;     (const c-string)))

;; (define notify-notification-show
;;   (foreign-lambda int "notify_notification_show"
;;     (c-pointer (struct _NotifyNotification))
;;     (c-pointer (c-pointer (struct _GError)))))

;; (define g-object-unref
;;   )
