(define raw-html
  (hc:call-with-input-request
   "https://web.archive.org/web/20170518154938/https://alexcabal.com/creating-the-perfect-gpg-keypair/"
   #f
   read-all))

(define (search-recursive sxml)
  (if (irregex-match "h." (symbol->string (car sxml)))
      sxml
      (1 )))
