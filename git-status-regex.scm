#!/usr/bin/csi -s
(import (chicken io)
        (chicken format)
        (chicken process)
        (srfi 1)
        (srfi 13)
        (chicken string)
        (chicken irregex))

(define-inline ($->string cmd)
  (call-with-input-pipe cmd (lambda (out) (read-string #f out))))

(define-syntax stylize
  (syntax-rules ()
    ((stylize color str)
     (format #f "%F{~a}~a%f" color str))
    ((stylize color symbol num)
     (if (not (zero? num))
         (format #f "%F{~a}~a~a%f" color symbol num)
         ""))))

(: main (-> (or string boolean)))
(define (main)
  (let ((status ($->string "git status --porcelain -b  2>/dev/null")))
    (if (string? status)
        (let* ((split (string-split status "\n"))
               (first-line (car split))
               (branch
                (cond ((substring-index "(no branch)" first-line)
                       (string-append
                        ":"
                        (string-trim-right
                         ($->string "git rev-parse --short HEAD"))))
                      ((substring-index "No commits yet" first-line)
                       "master")
                      (else (car (string-split ;; Isolate branch name
                                  ;; isolate branch/remote
                                  (car (string-split first-line "# "))
                                  ".")))))
               (ahead-behind-pos (substring-index "[" first-line))
               (ahead-pos (and ahead-behind-pos
                               (substring-index "ahead"
                                                first-line
                                                ahead-behind-pos)))
               (behind-pos (and ahead-behind-pos
                                (substring-index "behind"
                                                 first-line
                                                 ahead-behind-pos)))
               (ahead (if ahead-pos
                          (string->number (substring first-line
                                                     (+ ahead-pos 6)
                                                     (+ ahead-pos 7)))
                          0))
               (behind (if behind-pos
                           (string->number (substring first-line
                                                      (+ behind-pos 7)
                                                      (+ behind-pos 8)))
                           0))
               (files (cdr split))
               (status-list (map (cut substring <> 0 2) files))
               (staged (count (cut irregex-match
                                "A[ DM]|C[ DM]|D[ M]|M[ DM]|R[ DM]"
                                <>)
                              status-list))
               (conflicts (count (cut irregex-match
                                   "A[AU]|D[DU]|U[ADU]"
                                   <>)
                                 status-list))
               (modified (count (cut irregex-match
                                  " [DM]|A[DM]|C[DM]|M[DM]|R[DM]"
                                  <>)
                                status-list))
               (dirty (member "??" status-list)))
          (format #t
                  "(~a~a~a|~a~a~a~a~a) "
                  (stylize 10 branch)
                  (stylize 13 "↑" ahead)
                  (stylize 13 "↓" behind)
                  (stylize 12 "●" staged)
                  (stylize 9 "✖" conflicts)
                  (stylize 9 "✚" modified)
                  (if dirty "…" "")
                  (if (and (zero? staged)
                           (zero? conflicts)
                           (zero? modified)
                           (not dirty))
                      (stylize 10 "✓")
                      "")))
        #f)))

(main)
