#!/usr/bin/csi -s
(use irc
     srfi-18
     uri-common
     intarweb
     http-client
     (only medea read-json))

(form-urlencoded-separator "&")

(define (json-ref alist . keys)
  (define (alist? l)
    (list-of? pair?))
  (let loop ((alist alist)
             (keys keys))
    (cond ((null? keys)
           alist)
          ((procedure? (car keys))
           (loop ((car keys) alist) (cdr keys)))
          ((vector? alist)
           (loop (vector-ref alist (car keys)) (cdr keys)))
          (else
           (loop (alist-ref (car keys) alist) (cdr keys))))))

(define dbot:thread? #f)
(define dbot:commands '())

(define dbot:more
  )

(define dbot:con
  (irc:connection server: "127.0.0.1" nick: "d-bot") )

(irc:connect dbot:con)

(irc:join dbot:con "#happytimes")

;; (define (dbot:say str . chans)
;;   ( chans))

(define (dbot:restart)
  (when dbot:thread?
    (thread-terminate! dbot:thread))
  (define dbot:thread
    (thread-start!
     (lambda ()
       (irc:run-message-loop dbot:con debug: #t))))
  (set! dbot:thread? #t))

(define (dbot:command name proc
                      #!key
                      command
                      sender
                      receiver
                      body
                      code)
  (when (member name dbot:commands)
    (irc:remove-message-handler! dbot:con name))
  (irc:add-message-handler!
   dbot:con
   proc
   tag: name
   command: command
   sender: sender
   receiver: receiver
   body: body
   code: code)
  (set! dbot:commands (lset-adjoin equal? dbot:commands name)))

(define (dbot:uncommand name)
  (irc:remove-message-handler! dbot:con name)
  (set! dbot:commands (delete name dbot:commands)))

;; commands
(dbot:command
 'time
 (lambda (_)
   (irc:say dbot:con
            (seconds->string (current-seconds))
            "#happytimes"))
 body: ",time")

(define (wiki-search term)
  (define (make-wiki-uri search #!optional full)
    (let ((query-extra (if full
                           '((rvprop . content)
                             (prop . revisions))
                           '((prop . extracts)
                             (exintro . "")
                             (explaintext . "")))))
      (make-uri
       scheme: 'https
       host: "en.wikipedia.org"
       path: '(/ "w" "api.php")
       query: `((action . query)
                (titles . ,search)
                (format . json)
                (redirects . "")
                ,@query-extra))))
  (define (wiki-filter-markup str)
    (irregex-replace/all "(:?'''|'')(.*?)(:?'''|'')" str 2))
  (define (wiki-filter-link str)
    (let* ((regex (irregex "(\\[\\[(.*?)(?:\\|(.*?))?\\]\\])"))
           (match (irregex-search regex str)))
      (if match
          (begin
            (let ((link-path (irregex-match-substring match 2))
                  (newstr
                   (irregex-replace/all
                    regex
                    str
                    (if (irregex-match-substring match 3) 3 2))))
              (string-append newstr (format " https://en.wikipedia.org/wiki/~a"
                                            (irregex-replace/all " " link-path "_")))))
          str)))
  (let* ((search term)
         (response (with-input-from-request
                    (make-wiki-uri search) #f read-json))
         (result (json-ref response 'query 'pages cdar 'extract))
         (title (json-ref response 'query 'pages cdar 'title)))
    (if (irregex-search "may refer to:$" result)
        (let* ((response (with-input-from-request
                          (make-wiki-uri search 'full) #f read-json))
               (result (wiki-filter-markup
                        (json-ref response 'query 'pages cdar 'revisions 0 '*)))
               (items (filter (lambda (str) (string-prefix? "*" str))
                              (string-split result "\n"))))
          (string-join (map wiki-filter-link items) "\n"))
        (string-append (car (string-split result ".")) "."))))

(dbot:command
 'wiki
 (lambda (msg)
   (irc:say dbot:con
            (let* ((match (irregex-search ",wiki (.*)" (irc:message-body msg)))
                   (search (string-downcase (irregex-match-substring match 1))))
              (wiki-search search))
            "#happytimes"))
 body: ",wiki")
