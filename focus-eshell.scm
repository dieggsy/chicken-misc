(use scsh-process
     (prefix medea medea:)
     matchable)

(define (bspc-desktop-tree)
  (medea:read-json
   (string-chomp (run/string (bspc query -T -d)))))

(define (bspc-monitor-tree)
  (medea:read-json
   (string-chomp (run/string (bspc query -T -m)))))

(define (listify-recursively lst)
  (cond ((null? lst)
         '())
        ((not (list? (car lst)))
         (cons (car lst)
               (listify-recursively (cdr lst))))
        (else
         (cons (listify-recursively (car lst))
               (listify-recursively (cdr lst))))))

(define (tree-assoc key tree)
  (when (pair? tree)
    (match-let (((x . y) tree))
      (print "KEY:" key)
      (print "X: " x)
      (if (equal? x key)
          tree
          (or (tree-assoc key x) (tree-assoc key y))))))

(define alist?
  (list-of? pair?))

;; (define (alist-rec key tree)
;;   (if (alist? tree)
;;       (let (thing (alist-ref key tree))
;;         (if thing
;;             thing
;;             ()))
;;       #f))
;; (define (main)
;;   (let ((desktop-tree (bspc-desktop-tree)))
;;     ))
