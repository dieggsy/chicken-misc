#!/usr/bin/csi -s
(import (chicken io)
        (chicken format)
        (chicken process)
        (srfi 1)
        (srfi 13)
        (chicken string))

(define-inline ($->string cmd)
  (call-with-input-pipe
   cmd
   (lambda (out) (read-string #f out))))

(define-syntax stylize
  (syntax-rules ()
    ((stylize color str)
     (format #f "%{~C[~am%}~a%{~C[0m%}" #\esc color str #\esc))
    ((stylize color symbol num)
     (if (not (zero? num))
         (format #f
                 "%{~C[~am%}~a~a%{~C[0m%}"
                 #\esc
                 color
                 symbol
                 (number->string num)
                 #\esc)
         ""))))

(: main (-> (or string boolean)))
(define (main)
  (let ((status ($->string "git status --porcelain -b  2>/dev/null")))
    (if (string? status)
        (let* ((split (string-split status "\n"))
               (first-line (car split))
               (branch
                (cond ((string-contains first-line "(no branch)")
                       (string-append
                        ":"
                        (string-trim-right
                         ($->string "git rev-parse --short HEAD"))))
                      ((string-contains first-line "No commits yet")
                       "master")
                      (else (car (string-split ;; Isolate branch name
                                  ;; isolate branch/remote
                                  (car (string-split first-line "# "))
                                  ".")))))
               (ahead-behind-pos (string-index first-line #\[))
               (ahead-pos (and ahead-behind-pos
                               (string-contains first-line
                                                "ahead"
                                                ahead-behind-pos)))
               (behind-pos (and ahead-behind-pos
                                (string-contains first-line
                                                 "behind"
                                                 ahead-behind-pos)))
               (ahead (if ahead-pos
                          (string->number (substring first-line
                                                     (+ ahead-pos 6)
                                                     (+ ahead-pos 7)))
                          0))
               (behind (if behind-pos
                           (string->number (substring first-line
                                                      (+ behind-pos 7)
                                                      (+ behind-pos 8)))
                           0))
               (files (cdr split))
               (status-list (map (lambda (str)
                                   (substring str 0 2))
                                 files))
               (staged (count (lambda (str)
                                (or
                                 (string-prefix? "M" str)
                                 (string-prefix? "R" str)
                                 (string-prefix? "C" str)
                                 (and (string-prefix? "A" str)
                                      (not (or
                                            (string-suffix? "A" str)
                                            (string-suffix? "U" str))))
                                 (and (string-prefix? "D" str)
                                      (not (or
                                            (string-suffix? "D" str)
                                            (string-suffix? "U" str))))))
                              status-list))
               (dirty (member "??" status-list))
               (modified (count (lambda (str)
                                  (or (string-suffix? "M" str)
                                      (and
                                       (string-suffix? "D" str)
                                       (not (or (string-prefix? "D" str)
                                                (string-prefix? "U" str))))))
                                status-list))
               (conflicts (count (lambda (str)
                                   (or (string-contains str "U")
                                       (member str '("DD" "AA"))))
                                 status-list)))
          (format #t
                  "(~a~a~a|~a~a~a~a~a)~%"
                  (stylize 92 branch)
                  (stylize 95 "↑" ahead)
                  (stylize 95 "↓" behind)
                  (stylize 94 "●" staged)
                  (stylize 91 "✖" conflicts)
                  (stylize 91 "✚" modified)
                  (if dirty "…" "")
                  (if (and (zero? staged)
                           (zero? conflicts)
                           (zero? modified)
                           (not dirty))
                      (stylize 92 "✓")
                      "")))
        #f)))

(main)
