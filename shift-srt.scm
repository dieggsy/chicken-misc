(use irregex matchable numbers)

(define shift-seconds 7)

(define timestamp-regex
  "\\d{2}:\\d{2}:\\d{2},\\d{3}")

(define timestamp-line-regex
  (irregex (format "~a --> ~a" timestamp-regex timestamp-regex)))

(define (timestamp-shift timestamp seconds)
  ;; (print "TIMESTAMP")
  (match-let* ([(hr m s ms) (map string->number
                                 (string-split timestamp ":,"))]
               [ms-total (+ (* 1000 3600 hr)
                            (* 1000 60 m)
                            (* 1000 s)
                            ms)]
               [ms-final (- ms-total (* 1000 seconds))])
    (let*-values ([(hours ms-rem) (truncate/ ms-final 3600000)]
                  [(minutes ms-rem) (truncate/ ms-rem 60000)]
                  [(seconds ms-rem) (truncate/ ms-rem 1000)])
      (format "~a:~a:~a,~a"
              (
               hours)
              minutes
              seconds
              ms-rem))))

(define (shift-duration line seconds)
  ;; (print "DURATION")
  (if (irregex-match timestamp-line-regex line)
      (begin
        (printf "BEFORE: ~a~%" line)
        (match-let ([(time1 arrow time2) (string-split line " ")])
          (printf "AFTER: ~a ~a ~a~%"
                  (timestamp-shift time1 seconds)
                  arrow
                  (timestamp-shift time2 seconds))))
      #f))

(call-with-input-file "/home/dieggsy/Downloads/LOTR-FOTR-extended.srt"
  (lambda (orig-port)
    (let loop ((count 1)
               (line (read-line orig-port)))
      ;; (with-output-to-file "/tmp/LOTR-FOTR-shifted.srt")
      ;; (print count)
      ;; (lambda ())
      (if (eof-object? line)
          #f
          (begin
            (if (>= count 4685)
                (begin
                  (shift-duration line shift-seconds)
                  ;; (sleep 1)
                  )
                #f
                ;; (printf "~a~%" line)
                )
            ;; #:append
            ;; (sleep 1)
            (loop (+ count 1) (read-line orig-port)))))))
