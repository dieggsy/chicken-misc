#!/usr/bin/csi -s
(use (prefix i3 i3:)
     irregex)

(define inner-gap 30)

(define inverse-golden-ratio (/ 1 (/ (+ 1 (sqrt 5)) 2)))

(define (print-tree)
  (let* ((conn (i3:connect))
         (tree (i3:tree conn)))
    (pp tree)))

(define (resize-golden)
  (let* ((conn (i3:connect))
         (tree (i3:tree conn))
         (root-rect (alist-ref 'rect tree))
         (root-width (alist-ref 'width root-rect))
         (root-height (alist-ref 'height root-rect))
         (current-win
          (car
           (i3:filter-containers
            (lambda (node)
              (alist-ref 'focused node))
            tree)))
         (current-rect (alist-ref 'rect current-win))
         (current-width (alist-ref 'width current-rect))
         (current-height (alist-ref 'height current-rect))
         (grow-wide (inexact->exact
                     (round
                      (* 100
                         (/
                          (round
                           (- (* inverse-golden-ratio root-width) current-width))
                          root-width)))))
         (grow-high (inexact->exact
                     (round
                      (* 100
                         (/
                          (round
                           (- (* inverse-golden-ratio root-height) current-height))
                          root-width))))))
    (when (string-suffix? "off" (alist-ref 'floating current-win))
      (when (positive? grow-high)
        (i3:cmd conn (format "resize grow height 10 px or ~a ppt"
                             grow-high)))
      (when (positive? grow-wide)
        (i3:cmd conn (format "resize grow width 10 px or ~a ppt"
                             grow-wide))))))

(define (find-parent window-id tree)
  (let* ((res (i3:filter-containers
               (lambda (node)
                 (let ((nodes (alist-ref 'nodes node)))
                   (if (and nodes (not (null? nodes)))
                       (find
                        (lambda (node)
                          (= window-id (or (alist-ref 'id node) -1)))
                        (alist-ref 'nodes node))
                       #f)))
               tree)))
    (if (null? res) #f (car res))))

(define (set-layout)
  (let* ((conn (i3:connect))
         (tree (i3:tree conn))
         (current-win
          (car
           (i3:filter-containers
            (lambda (node)
              (alist-ref 'focused node))
            tree)))
         (parent (find-parent (alist-ref 'id current-win) tree)))
    (when (and parent
               (alist-ref 'rect parent)
               (not (string= (alist-ref 'layout parent) "tabbed"))
               (not (string= (alist-ref 'layout parent) "stacked")))
      (let* ((height (alist-ref 'height (alist-ref 'rect parent)))
             (width (alist-ref 'width (alist-ref 'rect parent)))
             (new-layout (if (> height width)
                             "vertical"
                             "horizontal")))
        (i3:cmd conn (format "split ~a" new-layout))))))

(define (main)
  ;; (print "hello")
  (let-values (((in out pid) (process "xprop -root -spy")))
    (let ((regex (irregex "^_NET_CLIENT_LIST_STACKING|^_NET_ACTIVE_WINDOW"))
          (last-line ""))
      (let loop ((last-line "")
                 (line (read-line in)))
        (cond ((string-null? line)
               #f)
              ((string= line last-line)
               ;; (thread-sleep! .1)
               (loop last-line (read-line in)))
              ((irregex-search regex line)
               (set-layout)
               ;; (resize-golden)
               ;; (thread-sleep! .1)
               (loop line (read-line in)))
              (else
               ;; (thread-sleep! .1)
               (loop line (read-line in)))))
      ;; (exit)
      )))

;; (main)
