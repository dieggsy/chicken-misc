(define (TRUE x y)
  x)

(define (FALSE x y)
  y)

(define (NOT b)
  (b FALSE TRUE))

(define (AND a b)
  (a b FALSE))

(define (OR a b)
  (a TRUE b))
